﻿using Microsoft.EntityFrameworkCore;
using WebApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Request> Requests { get; set; }
        public DbSet<Lifecycle> Lifecycles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Activ> Activs { get; set; }
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
    }
}

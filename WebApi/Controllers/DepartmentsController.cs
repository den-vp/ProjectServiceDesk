﻿using Microsoft.AspNetCore.Mvc;
using WebApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Abstractions.Repositories;
using WebApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    /// <summary>
    /// Отдел
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class DepartmentsController : ControllerBase
    {
        private readonly IRepository<Department> _departmentRepository;
        public DepartmentsController(IRepository<Department> departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }
        /// <summary>
        /// Получить данные всех отделов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DepartmentResponse>> GetDepartmentAsync()
        {
            var department = await _departmentRepository.GetAllAsync();

            var departmentModelList = department.Select(x =>
                new DepartmentResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                }).ToList();

            return departmentModelList;
        }

        /// <summary>
        /// Получить данные отдела по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<DepartmentResponse>> GetDepartmentByIdAsync(Guid id)
        {
            var department = await _departmentRepository.GetByIdAsync(id);

            if (department == null)
                return NotFound();

            var departmentModel = new DepartmentResponse()
            {
                Id = department.Id,
                Name = department.Name
            };

            return departmentModel;
        }

        [HttpPost]
        public async Task<ActionResult<DepartmentResponse>> CreateCustomerAsync(DepartmentResponse request)
        {
            var department = new Department()
            {
                Name = request.Name
            };

            await _departmentRepository.AddAsync(department);

            return CreatedAtAction(nameof(GetDepartmentByIdAsync), new { id = department.Id }, null);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, DepartmentResponse request)
        {
            var department = await _departmentRepository.GetByIdAsync(id);

            if (department == null)
                return NotFound();

            department.Name = request.Name;
            await _departmentRepository.UpdateAsync(department);
            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var department = await _departmentRepository.GetByIdAsync(id);

            if (department == null)
                return NotFound();

            await _departmentRepository.DeleteAsync(department);

            return NoContent();
        }
    }
}

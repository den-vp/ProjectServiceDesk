﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class DepartmentResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

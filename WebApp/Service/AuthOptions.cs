﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Service
{
    public class AuthOptions
    {
        public const string ISSUER = "EabrAuthServer"; // издатель токена
        public const string AUDIENCE = "EabrAuthClient"; // потребитель токена
        const string KEY = "eabrsupersecret_secretkey!2021!";   // ключ для шифрации
        public const int LIFETIME = 1; // время жизни токена - 1 минута
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}

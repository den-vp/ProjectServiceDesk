﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Domain
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

        public DbSet<TextField> TextFields { get; set; }
        public DbSet<ServiceItem> ServiceItems { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Lifecycle> Lifecycles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Activ> Activs { get; set; }
        public object IdentityUser { get; internal set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Department>().HasData(new Department
            {
                Id = new Guid("c48ab7b8-b9d5-4a27-b33f-a7ca697d934a"),
                Name = "УИТ"
            });

            modelBuilder.Entity<Department>().HasData(new Department
            {
                Id = new Guid("f2a9e75b-32d8-4238-9c7f-2cfd232f2593"),
                Name = "ОРОК"
            });

            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "0016aae4-11db-433f-b0a5-34c3e937206d",
                Name = "admin",
                NormalizedName = "admin"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                Id = "8aec76c5-9377-408c-bf58-92786282f5c0",
                Name = "",
                SurName = "",
                MiddleName = "",
                UserName = "admin",
                NormalizedUserName = "ADMIN",
                Email = "den_vp@mail.ru",
                NormalizedEmail = "DEN_VP@MAIL.RU",
                EmailConfirmed = true,
                DepartmentId = new Guid("c48ab7b8-b9d5-4a27-b33f-a7ca697d934a"),
                PasswordHash = new PasswordHasher<User>().HashPassword(null, "admin"),
                SecurityStamp = string.Empty
            }) ;

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "0016aae4-11db-433f-b0a5-34c3e937206d",
                UserId = "8aec76c5-9377-408c-bf58-92786282f5c0"
            });

            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "5ad97ac4-ea28-11eb-9a03-0242ac130003",
                Name = "user",
                NormalizedName = "user"
            });

            modelBuilder.Entity<User>().HasData(new User
            {
                Id = "e45a0455-949a-49c4-8b7f-4941bbb098eb",
                Name = "Test",
                SurName = "Test",
                MiddleName = "Test",
                UserName = "test",
                NormalizedUserName = "USER",
                Email = "vlasov_dp@eabr.org",
                NormalizedEmail = "VLASOV_DP@EABR.ORG",
                EmailConfirmed = true,
                DepartmentId = new Guid("f2a9e75b-32d8-4238-9c7f-2cfd232f2593"),
                PasswordHash = new PasswordHasher<User>().HashPassword(null, "test"),
                SecurityStamp = string.Empty
            });

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = "5ad97ac4-ea28-11eb-9a03-0242ac130003",
                UserId = "e45a0455-949a-49c4-8b7f-4941bbb098eb"
            });

            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "0016aae4-11db-433f-b0a5-34c3e9372061",
                Name = "moderator",
                NormalizedName = "moderator"
            });

            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "0016aae4-11db-433f-b0a5-34c3e9372062",
                Name = "executor",
                NormalizedName = "executor"
            });

            modelBuilder.Entity<TextField>().HasData(new TextField 
            { 
                Id = new Guid("bbf3ab8f-ecb3-4b36-8b20-ba4493132283"),
                CodeWord = "PageIndex",
                Title = "Главная"
            });

            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("861c56bf-3c8e-4d88-a706-4b5b32e81952"),
                CodeWord = "PageServices",
                Title = "Наши услуги"
            });

            modelBuilder.Entity<TextField>().HasData(new TextField
            {
                Id = new Guid("1dd4bd96-420e-45d8-be4d-d8aeadee026a"),
                CodeWord = "PageContacts",
                Title = "Контакты"
            });
            
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Domain;
using WebApp.Domain.Entities;
using WebApp.Models.ViewUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Areas.Admin.Controllers
{
    [/*Area("Admin"),*/ Authorize(Roles= "admin, moderator, executor")]
    public class UsersController : Controller
    {
        UserManager<User> userManager;
        private AppDbContext appDbContext;

        public UsersController(UserManager<User> userMgr, AppDbContext _appDbContext)
        {
            userManager = userMgr;
            appDbContext = _appDbContext;
        }

        // отображение списка пользователей
        [HttpGet]
        //public IActionResult Index() => View(userManager.Users.ToList());
        public ActionResult Index()
        {
            var users = userManager.Users.Include(u => u.Department).ToList();

            List<Department> departments = appDbContext.Departments.ToList();
            //Добавляем в список возможность выбора всех
            departments.Insert(0, new Department { Name = "Все", Id = new Guid("00000000-0000-0000-0000-000000000000") });
            ViewBag.Departments = new SelectList(departments, "Id", "Name");

            return View(users);
        }

        // поиск пользователей по департамену и статусу
        [HttpPost]
        public ActionResult Index(Guid department)
        {
            IEnumerable<User> allUsers = null;
            if (department == new Guid("00000000-0000-0000-0000-000000000000"))
            {
                return RedirectToAction("Index");
            }
            if (department != new Guid("00000000-0000-0000-0000-000000000000"))
            {
                allUsers = from user in userManager.Users.Include(u => u.Department)
                           where user.DepartmentId == department
                           select user;
            }

            List<Department> departments = appDbContext.Departments.ToList();
            //Добавляем в список возможность выбора всех
            departments.Insert(0, new Department { Name = "Все", Id = new Guid("00000000-0000-0000-0000-000000000000") });
            ViewBag.Departments = new SelectList(departments, "Id", "Name");

            return View(allUsers.ToList());
        }
        public IActionResult Create()
        {
            SelectList departments = new SelectList(appDbContext.Departments, "Id", "Name");
            ViewBag.Departments = departments;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User { UserName = model.Login, Name = model.Name, SurName = model.SurName, MiddleName = model.MiddleName, Email = model.Email, DepartmentId = model.DepartmentId };
                var result = await userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
                SelectList departments = new SelectList(appDbContext.Departments, "Id", "Name");
                ViewBag.Departments = departments;
            }
            return View(model);
        }

        public async Task<IActionResult> Edit(string id)
        {
            User user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            EditUserViewModel model = new EditUserViewModel { Id = user.Id, Name = user.Name, SurName = user.SurName, MiddleName = user.MiddleName, Email = user.Email, Login = user.UserName, DepartmentId = (Guid)user.DepartmentId };
            SelectList departments = new SelectList(appDbContext.Departments, "Id", "Name");
            ViewBag.Departments = departments;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    user.Name = model.Name;
                    user.SurName = model.SurName;
                    user.MiddleName = model.MiddleName;
                    user.Email = model.Email;
                    user.DepartmentId = model.DepartmentId;
                    var result = await userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }

                    }
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            User user = await userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index");
        }
    }
}

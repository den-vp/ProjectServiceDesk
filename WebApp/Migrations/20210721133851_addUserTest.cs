﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class addUserTest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0016aae4-11db-433f-b0a5-34c3e937206d",
                column: "ConcurrencyStamp",
                value: "92057fa1-2606-4252-b492-5c9fc65a70bc");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "5ad97ac4-ea28-11eb-9a03-0242ac130003", "4659bf9b-6337-4db4-bb39-1b5905e3df3b", "user", "USER" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8aec76c5-9377-408c-bf58-92786282f5c0",
                columns: new[] { "ConcurrencyStamp", "NormalizedEmail", "PasswordHash" },
                values: new object[] { "2ba31a00-d0d3-4e6a-917d-6f8edf41cfe0", "DEN_VP@MAIL.RU", "AQAAAAEAACcQAAAAENsUc3cn4PKmPdQ55K3qCCJMusJXgyO9eIS9wbpvXJMmCZz1QzCLqOU1ZhpYMKwixg==" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "MiddleName", "Name", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "SurName", "TwoFactorEnabled", "UserName" },
                values: new object[] { "e45a0455-949a-49c4-8b7f-4941bbb098eb", 0, "ae86f524-bb17-44c4-878c-f1a0cdd9ba04", "vlasov_dp@eabr.org", true, false, null, null, "User", "VLASOV_DP@EABR.ORG", "USER", "AQAAAAEAACcQAAAAECuCvb8T99L1vKwf5d1pW7tBaQdoONvFhPRJ6B/0XWvhWEtL84+/87lDWMjcN95aSg==", null, false, "", "Тест", false, "Пользователь" });

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("1dd4bd96-420e-45d8-be4d-d8aeadee026a"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 13, 38, 51, 343, DateTimeKind.Utc).AddTicks(2082));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("861c56bf-3c8e-4d88-a706-4b5b32e81952"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 13, 38, 51, 343, DateTimeKind.Utc).AddTicks(2051));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("bbf3ab8f-ecb3-4b36-8b20-ba4493132283"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 13, 38, 51, 343, DateTimeKind.Utc).AddTicks(104));

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "5ad97ac4-ea28-11eb-9a03-0242ac130003", "e45a0455-949a-49c4-8b7f-4941bbb098eb" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "5ad97ac4-ea28-11eb-9a03-0242ac130003", "e45a0455-949a-49c4-8b7f-4941bbb098eb" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5ad97ac4-ea28-11eb-9a03-0242ac130003");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "e45a0455-949a-49c4-8b7f-4941bbb098eb");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0016aae4-11db-433f-b0a5-34c3e937206d",
                column: "ConcurrencyStamp",
                value: "52faa988-7a4c-4288-8477-82db9110947e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8aec76c5-9377-408c-bf58-92786282f5c0",
                columns: new[] { "ConcurrencyStamp", "NormalizedEmail", "PasswordHash" },
                values: new object[] { "93595b40-35c1-44f9-81cf-9903180f5379", "DEN_VP@MAIL>RU", "AQAAAAEAACcQAAAAEIjvwuFg6PceV9CAwF+Uo26mmkc5Ac+B0ecccXqYvTFAth8pnGhrV8wKB1bzvt2aiw==" });

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("1dd4bd96-420e-45d8-be4d-d8aeadee026a"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 11, 36, 25, 380, DateTimeKind.Utc).AddTicks(5929));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("861c56bf-3c8e-4d88-a706-4b5b32e81952"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 11, 36, 25, 380, DateTimeKind.Utc).AddTicks(5896));

            migrationBuilder.UpdateData(
                table: "TextFields",
                keyColumn: "Id",
                keyValue: new Guid("bbf3ab8f-ecb3-4b36-8b20-ba4493132283"),
                column: "DateAdded",
                value: new DateTime(2021, 7, 21, 11, 36, 25, 380, DateTimeKind.Utc).AddTicks(3962));
        }
    }
}
